rgarrigue.yum-repositories
==========================

Install yum repositories like EPEL or remi.

Note, this kind of stuff can't be 100% generic. There's a specific remi GPG key for repositories from 2017 OSs onward like Fedora 26, EPEL ofc won't work on Fedora, etc.

Requirements
------------

None.

Role Variables
--------------

By default only EPEL repository is installed. Here's an example with EPEL, remi, rpmfusion free & nonfree for CentOS. Repositories will be installed in the top-down order, which is important in this example since rpmfusion-nonfree requires rpmfusion-free requires epel.

```yaml
yum_repositories:
  - rpm_url: "https://dl.fedoraproject.org/pub/epel/epel-release-latest-{{ ansible_distribution_major_version }}.noarch.rpm"
    gpg_key_url: "/etc/pki/rpm-gpg/RPM-GPG-KEY-EPEL-{{ ansible_distribution_major_version }}"
    repo_file_path: "/etc/yum.repos.d/epel.repo"

  - rpm_url: "http://rpms.remirepo.net/enterprise/remi-release-{{ ansible_distribution_major_version }}.rpm"
    gpg_key_url: "http://rpms.remirepo.net/RPM-GPG-KEY-remi"
    repo_file_path: "/etc/yum.repos.d/remi.repo"

  - rpm_url: "https://download1.rpmfusion.org/free/el/rpmfusion-free-release-{{ ansible_distribution_major_version }}.noarch.rpm"
    gpg_key_url: "https://rpmfusion.org/keys?action=AttachFile&do=get&target=RPM-GPG-KEY-rpmfusion-free-el-{{ ansible_distribution_major_version }}"
    repo_file_path: "/etc/yum.repos.d/rpmfusion-free-updates.repo"
  - rpm_url: "https://download1.rpmfusion.org/nonfree/el/rpmfusion-nonfree-release-{{ ansible_distribution_major_version }}.noarch.rpm"
    gpg_key_url: "https://rpmfusion.org/keys?action=AttachFile&do=get&target=RPM-GPG-KEY-rpmfusion-nonfree-el-{{ ansible_distribution_major_version }}"
    repo_file_path: "/etc/yum.repos.d/rpmfusion-nonfree-updates.repo"
```

Dependencies
------------

None.

Example Playbook
----------------

```yaml
- hosts: fedoras
  vars:
    yum_repositories:
    - rpm_url: "https://download1.rpmfusion.org/free/fedora/rpmfusion-free-release-{{ ansible_distribution_major_version }}.noarch.rpm"
      gpg_key_url: "https://rpmfusion.org/keys?action=AttachFile&do=get&target=RPM-GPG-KEY-rpmfusion-free-fedora-{{ ansible_distribution_major_version }}"
      repo_file_path: "/etc/yum.repos.d/rpmfusion-free-updates.repo"
  roles:
     - rgarrigue.yum-repositories
```

Tests
-----

This role is tested using [Molecule](https://molecule.readthedocs.io/en/latest/) & [Travis CI](http://travis-ci.org/), installing remi repository [![Build Status](https://travis-ci.org/rgarrigue/ansible-role-yum-repositories.svg?branch=master)](https://travis-ci.org/rgarrigue/ansible-role-yum-repositories)

For manual test, [install molecule deps](https://github.com/rgarrigue/ansible-role-yum-repositories/blob/master/molecule/default/INSTALL.rst) then run `molecule test`

License
-------

GPLv3

Author Information
------------------

Rémy Garrigue
